angular.module('almanah', []).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
})
.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];

        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
})
.filter('truncate', function(){
        return function(input){
            var truncated_str = input.split(' ').slice(0, 20);
            truncated_str.push('...');
            return truncated_str.join(' ');
        }
});

function Playground($scope, $http) {
    $scope.events = [];

    $scope.get_events = function(){
        $http.get('/events/' + $scope.events.length).success(function(data){
            $scope.events.push.apply($scope.events, data.events);
        });
    };

    $scope.get_events();
}
