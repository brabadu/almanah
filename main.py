from random import choice

from flask import Flask, jsonify
from flask import render_template

from lorem import get_lorem


IMG_PLACEHOLDER_SIZES = xrange(100, 501, 50)

app = Flask(__name__)


@app.route("/")
def hello():
    return render_template('index.html')


@app.route("/events")
@app.route("/events/<int:start>")
def get_events(start=None):
    start = start or 0
    return jsonify({
        'meta': 'Starting events from event #%d' % start,
        'events': [{
            'title': 'Title #%d' % (i + 1),
            'body': get_lorem(),
            'link': 'http://google.com',
            'image': 'http://placekitten.com/%d/%d' % (
                        choice(IMG_PLACEHOLDER_SIZES),
                        choice(IMG_PLACEHOLDER_SIZES),
                        )
        } for i in xrange(start, start + 5)]
    })


if __name__ == "__main__":
    app.run(port=8000, debug=True)
