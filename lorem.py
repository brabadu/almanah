import random


WORDS_LIST = """
In odio turpis, scelerisque id scelerisque ac, ultricies at neque. Donec sapien purus, interdum et vulputate eget, laoreet vitae eros. Sed egestas lorem vitae enim pulvinar faucibus. Etiam quis sem urna, quis vulputate erat. Curabitur ut risus orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempor ipsum sed felis tempus sagittis. Suspendisse potenti. Aliquam eget tellus eu eros blandit feugiat egestas quis leo. Nulla a elit quis turpis bibendum varius ultricies vel purus. In consectetur ornare feugiat. Nam pharetra felis eu mauris aliquet lobortis. Vestibulum ornare nulla eget ante accumsan eu fringilla ante pharetra. Etiam et auctor nulla.

In semper elementum mi sed mollis. Nam accumsan eleifend elit quis aliquet. Morbi ullamcorper vulputate urna, ut porttitor tortor tempor at. Donec tempor pellentesque augue nec tristique. Vivamus dolor quam, laoreet at ullamcorper id, elementum in sapien. Nulla facilisi. Morbi aliquam iaculis turpis quis hendrerit. Aenean commodo, elit at suscipit imperdiet, justo libero adipiscing massa, et pretium lacus lacus et justo. Vivamus a nulla mi, quis consequat ipsum. Duis ut ante a enim pharetra volutpat sed vitae tellus. Phasellus est nisi, venenatis ut iaculis eu, vestibulum non mauris. Phasellus feugiat tincidunt dui, sed convallis lorem convallis non. Nunc sagittis justo sollicitudin ante consectetur ac dapibus erat interdum. Ut sed ultrices eros.

Sed adipiscing suscipit ante nec dignissim. Quisque in volutpat mi. In hac habitasse platea dictumst. Nulla porttitor augue erat, id accumsan dui. In hac habitasse platea dictumst. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec non enim sapien, id consequat felis. Duis nec urna neque. Nam hendrerit aliquam neque ac malesuada. Nam non mi ornare nulla convallis facilisis. Phasellus sed felis vel leo faucibus porttitor nec nec purus.

Proin in nisl purus. Sed viverra luctus eros, sed blandit risus iaculis vitae. Praesent nec mauris sit amet elit auctor dignissim dignissim vel quam. Aliquam ipsum tortor, volutpat ac interdum vel, eleifend at massa. Ut fermentum risus sit amet felis condimentum id fringilla mauris euismod. Suspendisse posuere accumsan orci, eu mollis mi tempus a. Vestibulum vulputate dolor sed justo facilisis tempor. Etiam lacinia viverra facilisis. Proin gravida arcu nec enim lacinia pharetra rutrum dolor porta. Ut nibh magna, rutrum egestas gravida at, accumsan vitae tellus. Curabitur vestibulum diam vel neque eleifend sollicitudin pharetra neque suscipit. Nam porta aliquet lectus vel lobortis. Praesent cursus cursus facilisis. Ut nec leo eget nulla consequat ultrices. Curabitur in tincidunt magna. In at dolor erat, quis sodales nibh.

Integer eget orci sit amet tellus pretium condimentum. Sed ut quam eleifend arcu gravida venenatis at non ante. In adipiscing vestibulum fringilla. Suspendisse eu nulla libero. Proin sagittis faucibus quam, eget auctor nibh elementum placerat. Donec viverra leo ultricies libero pharetra in mollis augue rutrum. Aliquam imperdiet, dolor a egestas varius, sapien velit dictum justo, eget mollis enim sem ac nisl. Suspendisse sed tortor ligula, vitae commodo neque. Cras vitae purus a felis convallis auctor at eget velit. Proin in turpis id tortor semper convallis. Suspendisse ut vulputate lorem. Phasellus laoreet vulputate tempor.""".split()


def get_lorem(words=100, paragraphs=3):
    """
    Gernerates placeholder text aka Lorem ipsum

    Arguments:
    - `words`: maximum words in paragraph
    - `paragraphs`: how many paragraphs to give
    """

    return '\n'.join(
        ' '.join(random.choice(WORDS_LIST) for j in xrange(random.randint(3, max(3, words))))
           for i in xrange(paragraphs)
    )
